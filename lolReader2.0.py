import discord
from discord.ext import commands
from riotwatcher import RiotWatcher
import json
import urllib.request
import tmdbsimple as tmdb


if True:
    tmdb.API_KEY = '1e52fc2054f384897d03a1c60230a373'
    key = "RGAPI-23f3648b-e9e1-419d-82e0-991ef9421677"
    TOKEN = 'NDMzMTgwMTU3NzUxMzI4NzY4.XQjp3w.yrjtZOapx1_yqmbgZuASSUSkaLQ'

    watcher = RiotWatcher(key)
    regionDict = {"eune": "eun1", "euw": "euw1"}
    league_version = "9.15"
    json_file_url = f'http://ddragon.leagueoflegends.com/cdn/{league_version}/data/en_US/champion.json'

    bot = commands.Bot(command_prefix="lol ")
    bot.remove_command("help")


async def get_rank(region, sum_name):
    if region == "eune":
        region = "eun1"

    elif region == "euw":
        region = "euw1"

    response = watcher.summoner.by_name(region, sum_name)
    id = response["id"]
    print(id)
    actual_sum_name = response["name"]
    picID = response["profileIconId"]
    pic_url = f"http://ddragon.leagueoflegends.com/cdn/{league_version}/img/profileicon/{str(picID)}.png"

    get_league = watcher.league.by_summoner(region, id)
    if get_league[0]['queueType'] == 'RANKED_FLEX_SR':
        n = 1
    elif get_league[0]['queueType'] == 'RANKED_TFT':
        n = 1
    else:
        n = 0
    tier = get_league[n]["tier"]
    rank = get_league[n]["rank"]
    lp = get_league[n]["leaguePoints"]
    wins = get_league[n]["wins"]
    losses = get_league[n]["losses"]
    total_games = wins + losses
    winrate = round((wins / total_games) * 100, 2)
    if tier == "GRANDMASTER" or tier == "CHALLENGER":
        rank = ""

    rank_url = discord.File(f'division_emblems/Emblem_{tier.capitalize()}.png', filename='image.png')
    # http://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/content/src/leagueclient/rankedcrests/

    embed = discord.Embed(title="Summary of " + actual_sum_name, color=0xFF0000)
    embed.set_thumbnail(url=pic_url)
    embed.add_field(name="Tier: ", value=tier.capitalize() + " " + rank)
    embed.add_field(name="Games played", value=total_games, inline=True)
    embed.add_field(name="Lp: ", value=lp, inline=True)
    embed.add_field(
        name="Wins : Losses", value=str(wins) + " : " + str(losses), inline=False
    )
    embed.add_field(name="Winratio", value=str(winrate)+'%', inline=True)
    embed.set_image(url='attachment://image.png')
    return embed, rank_url


async def tft_rank(region, sum_name):
    if region == "eune":
        region = "eun1"

    elif region == "euw":
        region = "euw1"

    response = watcher.summoner.by_name(region, sum_name)
    id = response["id"]
    print(id)
    actual_sum_name = response["name"]
    picID = response["profileIconId"]
    pic_url = f"http://ddragon.leagueoflegends.com/cdn/{league_version}/img/profileicon/{str(picID)}.png"

    get_league = watcher.league.by_summoner(region, id)
    if get_league[0]['queueType'] == 'RANKED_FLEX_SR':
        n = 1
    elif get_league[0]['queueType'] == 'RANKED_SOLO_5x5':
        n = 1
    else:
        n = 0
    tier = get_league[n]["tier"]
    rank = get_league[n]["rank"]
    lp = get_league[n]["leaguePoints"]
    wins = get_league[n]["wins"]
    losses = get_league[n]["losses"]
    total_games = wins + losses
    winrate = round((wins / total_games) * 100, 2)
    if tier == "GRANDMASTER" or tier == "CHALLENGER":
        rank = ""

    rank_url = discord.File(f'division_emblems/Emblem_{tier.capitalize()}.png', filename='image.png')
    #http://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/content/src/leagueclient/rankedcrests/

    embed = discord.Embed(title="Summary of " + actual_sum_name, color=0xFF0000)
    embed.set_thumbnail(url=pic_url)
    embed.add_field(name="Tier: ", value=tier.capitalize() + " " + rank)
    embed.add_field(name="Games played", value=total_games, inline=True)
    embed.add_field(name="Lp: ", value=lp, inline=True)
    embed.add_field(
        name="Wins : Losses", value=str(wins) + " : " + str(losses), inline=False
    )
    embed.add_field(name="Winratio", value=str(winrate)+'%', inline=True)
    embed.set_image(url='attachment://image.png')
    return embed, rank_url

async def mastery_points(region, sum_name):
    if region == "eune":
        region = "eun1"

    elif region == "euw":
        region = "euw1"
    response = watcher.summoner.by_name(region, sum_name)
    id = response["id"]
    actual_sum_name = response["name"]
    picID = response["profileIconId"]
    pic_url = f"http://ddragon.leagueoflegends.com/cdn/{league_version}/img/profileicon/{str(picID)}.png"

    points = watcher.champion_mastery.scores_by_summoner(region, id)

    embed = discord.Embed(title=f'Combined mastery level of {actual_sum_name}', description='', color=0xFF0000)
    embed.set_thumbnail(url=pic_url)
    embed.add_field(name=str(points), value='levels')
    return embed


async def champ_mastery(region, sum_name, champ):
    if region == "eune":
        region = "eun1"

    elif region == "euw":
        region = "euw1"

    response = watcher.summoner.by_name(region,sum_name)
    id = response['id']
    print(id)
    actual_sum_name = response['name']
    picID = response["profileIconId"]
    print(picID)

    pic_url = f"http://ddragon.leagueoflegends.com/cdn/{league_version}/img/profileicon/{str(picID)}.png"
    champ_pic_url = f'http://ddragon.leagueoflegends.com/cdn/{league_version}/img/champion/{champ.capitalize()}.png'

    with urllib.request.urlopen(json_file_url) as file:
        json_data = json.loads(file.read())
    champ_id = json_data['data'][champ.capitalize()]['key']
    points = watcher.champion_mastery.by_summoner_by_champion(region, id, champ_id)['championPoints']
    level = watcher.champion_mastery.by_summoner_by_champion(region, id, champ_id)['championLevel']

    embed = discord.Embed(title=f'Masterypoints for {actual_sum_name} on {champ.capitalize()}', description='',
                          color=0xFF0000)
    embed.set_thumbnail(url=pic_url)
    embed.add_field(name='Masterypoints: ', value=str(points))
    embed.add_field(name='Mastery level: ', value=str(level))
    embed.set_image(url=champ_pic_url)
    return embed


async def movie_info(title):
    search = tmdb.Search()
    response = search.movie(query=title)
    s = search.results[0]  # finder første søgereslutat
    movie_id = s['id']                              #Her findes user reviews
    rating = s['vote_average']
    title = s['title']
    year, month, date = s['release_date'].split('-')
    content = f'**Title**: {title}\n**Releasedate**: {date}-{month}-{year}'

    movie = tmdb.Movies(movie_id)                   #her findes information om film-id'et
    response = movie.info()
    runtime = movie.runtime
    content += f'\n**Run time**: {runtime} minutes'
    genres = movie.genres                           #genrer findes
    content += f'\n**Genres** associated with {title}:\n'
    for name in genres:
        content += ' - *' + name['name'] + '*\n'      #Den looper rundt, og finder alle genrer

    budget = movie.budget
    budget = f'{budget:,}'                          #Her findes film budgetttet
    content += f'**Budget**: {budget} USD\n'
    revenue = movie.revenue
    revenue = f'{revenue:,}'
    content += f'**Total revenue**: {revenue} USD\n'
    content += f'The viewers have rated this movie {str(rating)}/10'

    return content


async def movie_overwiev(title):
    search = tmdb.Search()
    response = search.movie(query=title)
    s = search.results[0]
    title = s['title']
    adult = s['adult']
    movie_id = s['id']

    movie = tmdb.Movies(movie_id)
    response = movie.info()
    review = movie.overview

    return f'Overview of {title}:\n{review}'


async def series_overview(title):
    search = tmdb.Search ()  # Storer search funktionen i en variabel
    response = search.tv (
        query=title
    )
    s = search.results[0]
    series_id = s["id"]
    title = s["name"]
    overview = s['overview']
    return f'Overview of {title}:\n{overview}'


async def series_info(series):
    search = tmdb.Search()  # Storer search funktionen i en variabel
    response = search.tv(
        query=series
    )  # Bruger search variablen til at søge efter serie og laver en liste med resultater
    s = search.results[0]  # finder det første søge resultat
    series_id = s["id"]
    title = s["name"]
    print(s)

    content = f"**Title**: {title}"  # opretter variablen der skal returnes
    series = tmdb.TV(series_id)  # Finder Serien, ud af id'et
    response = series.info()

    content += "\n**Created by**: "
    created_by = series.created_by  # finder list over creatros
    for name in created_by:  # Her splittes listen op, og efter hver creator...
        content += f'\n - *{name["name"]}*'  # laves der en tilføjelse til content

    networks = series.networks  # finder listen over networks
    content += f"\n**Networks associated with {title}**:"
    for (network) in networks:  # Hver gang der er et nyt network i listen over networks...
        content += f'\n - *{network["name"]}*'  # Tilføjer den network til content

    seasons = series.number_of_seasons  # finder antal af sæsonner
    content += f"\nThe show contains **{seasons} seasons**"

    runtime = series.episode_run_time  # finder liste over runtime
    if len(runtime) > 1:  # hvis der er mere end en runtime
        for time in runtime:  # kører den et loop, der finder dem
            content += (
                f" and each episode has a **runtime** of *{time} minutes*"
            )  # og tilføjer til contetnt
    else:
        content += (
            f" and each episode has a **runtime** of *{runtime[0]} minutes*"
        )  # hvis der kun er en runtime, tilføjer den det bare

    status = series.status  # finder status for serien
    content += f'\n**Status**: {status}'

    rating = series.vote_average
    content += f'\nThe show has an **average score** of *{rating}/10* on The Movie Database'

    content += f'\n**Genres** associated with {title}'
    genres = series.genres
    for genre in genres:
        content += f'\n - *{genre["name"]}*'

    #print(response)
    #print("\n")

    return content


async def martin_review(title):
    with open('martin_series.json', 'r') as data:
        info = json.load(data)
    resume = info[title.upper()]
    return resume


async def martin_dump(title, review):
    pass
    with open('martin_series.json', 'a') as data:
        info = json.dump()


@bot.event
async def on_ready():
    print("ready")
    print(discord.__version__)
    print(bot.user.id)
    await bot.change_presence(activity=discord.Game('Type \'lol help\' for help on how to use my commands'))


@bot.command()
async def help(ctx):
    embed = discord.Embed(title='', description='In this list, you can see the subjects of my commands', color=0xFF0000)
    embed.add_field(name='League of Legends', value='To see available commands type "lol league"')
    embed.add_field(name='Movies and TV-series', value='To see available commands type "lol media"')
    await ctx.send (embed=embed)


@bot.command()
async def league(ctx):
    embed = discord.Embed(title='',
                          description='This is a list of the commands I\'m able to respond to,'
                                      'that are related to League of Legends',
                          color=0xFF0000)
    embed.add_field(name='lol rank',
                    value='This command will make me reply with the rank and division of the given summoner',
                    inline=True)
    embed.add_field(name='Example:', value='lol rank euw Tr3bla', inline=True)
    embed.add_field(name='\u200b', value='\u200b', inline=False)
    embed.add_field(name='lol mastery',
                    value='This command will make me reply with the combined mastery level of the given summoner')
    embed.add_field(name='Example:', value='lol mastery eune DenSortePumba')
    embed.add_field(name='\u200b', value='\u200b', inline=False)
    embed.add_field(name='lol champion_mastery',
                    value='This command will make me reply with the mastery level '
                          + 'and masterypoints of a specific champion')
    embed.add_field(name='Example', value='lol champion_mastery na Draven Tyler1')

    await ctx.send(embed=embed)


@bot.command()
async def media(ctx):
    embed = discord.Embed(title='',
                          description='This is a list of commands I\'m able to respond to,'
                                      'that are related to movies or TV-series',
                          color=0xFF0000)
    embed.add_field(name='lol movie',
                    value='This command searches "The Movie Database" for information about a specific movie!')
    embed.add_field(name='Example', value=' lol movie Star Wars: The Force Awakens')
    embed.add_field(name='\u200b', value='\u200b', inline=False)
    embed.add_field(name='lol series',
                    value='This command searches "The Movie Database" for information about a specific TV-series')
    embed.add_field(name='Example', value='lol series Game of Thrones')
    embed.add_field(name='\u200b', value='\u200b', inline=False)
    embed.add_field(name='lol movie_resume',
                    value='This command gives a short overview of a specific movie', inline=False)
    embed.add_field(name='Example', value='lol movie_resume Dear John')
    embed.add_field(name='\u200b', value='\u200b', inline=False)
    embed.add_field(name='lol series_resume', value='This command gives a short overview of a specific TV-series')
    embed.add_field(name='Example:', value='lol series_resume Lucifer')

    await ctx.send (embed=embed)


@bot.command()
async def rank(ctx, region, *, sum_name):
    content, rank_url = await get_rank(region, sum_name)
    await ctx.send(file=rank_url, embed=content)


@bot.command()
async def tft(ctx, region, *, sum_name):
    content, rank_url = await tft_rank(region, sum_name)
    await ctx.send(file=rank_url, embed=content)


@bot.command()
async def mastery(ctx, region, *, sum_name):
    content = await mastery_points(region, sum_name)
    await ctx.send(embed=content)


@bot.command()
async def champion_mastery(ctx, region, champ, *, sum_name):
    content = await champ_mastery(region, sum_name, champ)
    await ctx.send(embed=content)


@bot.command()
async def movie(ctx, *, title):
    content = await movie_info(title)
    await ctx.send(content)


@bot.command()
async def series(ctx, *, title):
    content = await series_info(title)
    await ctx.send(content)


@bot.command()
async def movie_resume(ctx, *, title):
    content = await movie_overwiev(title)
    await ctx.send(content)


@bot.command()
async def series_resume(ctx, *, title):
    content = await series_overview(title)
    await ctx.send(content)


@bot.command()
async def martin_serie(ctx, *, title):
    content = await martin_review(title)
    await ctx.send(content)


@bot.command()
async def add(ctx, *, title):
    print('working')


@bot.command()
async def test(ctx):
    pass

bot.run(TOKEN)
