from riotwatcher import RiotWatcher
import json
import urllib.request
key = "RGAPI-5903ba41-6179-4ae4-b882-e72127c4bff7"
watcher = RiotWatcher(key)
name = 'densortepumba'
region = 'eun1'
champ = 'aatrox'
league_version = '9.12.1'
json_file_url = f'http://ddragon.leagueoflegends.com/cdn/{league_version}/data/en_US/champion.json'

with urllib.request.urlopen(json_file_url) as json_data:
    data = json.loads(json_data.read())
    champ_id = data['data'][champ.capitalize()]['key']
    print(champ_id)
    x = watcher.summoner.by_name(region, name)
    id = x['id']
    print(id)
    summoner_mastery = watcher.champion_mastery.by_summoner_by_champion(region,id, champ_id)#['championPoints']
    print(summoner_mastery)